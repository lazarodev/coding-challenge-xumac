package lazaro.io.codechallengexumac;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.todkars.shimmer.ShimmerRecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import lazaro.io.codechallengexumac.character.Character;
import lazaro.io.codechallengexumac.character.CharacterAdapter;
import lazaro.io.codechallengexumac.character.model.Characters;
import lazaro.io.codechallengexumac.utils.AppController;
import lazaro.io.codechallengexumac.utils.Connectivity;
import lazaro.io.codechallengexumac.utils.Utils;

public class MainActivity extends AppCompatActivity {

    private ShimmerRecyclerView rvCharacters;
    private final ArrayList<Character> characters = new ArrayList<>();
    private final Utils utils = new Utils();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setTitle(R.string.bb_characters);

        // Init recycler view
        rvCharacters = findViewById(R.id.rv_characters);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvCharacters.setLayoutManager(linearLayoutManager);
        rvCharacters.setHasFixedSize(true);
        rvCharacters.setItemAnimator(new DefaultItemAnimator());

        // Verify first if there's internet connection
        if (!Connectivity.isConnected(this)) {
            utils.showWarningMessage(this, getString(R.string.no_internet), getString(R.string.connect_to_internet));
        } else {
            retrieveCharacters();
        }
    }

    // Method to retrieve the characters consuming api rest from breaking bad series.
    private void retrieveCharacters() {

        rvCharacters.showShimmer();

        // Initialize a new JsonArrayRequest instance
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                Request.Method.GET, Utils.CHARACTERS_WS,
                null,
                response -> {
                    Log.d("Respponse: ", response.toString());
                    try {
                        for (int i = 0; i < response.length(); i++) {
                            // Get current json object
                            JSONObject character = response.getJSONObject(i);

                            // Get each characters values
                            int charID = character.getInt("char_id");
                            String name = character.getString("name");
                            String nickname = character.getString("nickname");
                            String urlImage = character.getString("img");
                            JSONArray occupations = character.getJSONArray("occupation");
                            String occupation = occupations.getString(0);
//                            Log.d("Ocupations", occupations.toString());
                            Log.d("Ocupation", occupation);
                            String status = character.getString("status");
                            String portrayed = character.getString("portrayed");

                            // Verify if this character is currently in the favorites character database
                            boolean isCurrentlyFav = new Characters().isFavorite(charID);

                            // Set all the favorite characters at the top of the list
                            if (isCurrentlyFav) {
                                characters.add(0, new Character(charID, name, nickname, occupation, status, portrayed, urlImage));
                            } else {
                                characters.add(new Character(charID, name, nickname, occupation, status, portrayed, urlImage));
                            }
                        }

                        setupAdapter(characters);
                        rvCharacters.hideShimmer();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.e("VOLLEY ERROR: " + error.getMessage());
                    }
                }
        );

        RetryPolicy policy = new DefaultRetryPolicy(6000, 5, 1);
        jsonArrayRequest.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(jsonArrayRequest);
    }

    // Method to set the instance of our adapter to match with our characters array list
    private void setupAdapter(ArrayList<Character> sponsors) {
        this.rvCharacters.setAdapter(new CharacterAdapter(sponsors));
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        // Clear previous data on list
        characters.clear();
        retrieveCharacters();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}