package lazaro.io.codechallengexumac.utils;

import android.content.Context;

import com.example.flatdialoglibrary.dialog.FlatDialog;

import lazaro.io.codechallengexumac.R;

public class Utils {

    // Declaring and init recurring constants and methods
    public static final String PREFS_NAME = "MyPrefsFile";
    public static final String CHARACTERS_WS = "https://www.breakingbadapi.com/api/characters?limit=100";

    public void showWarningMessage(Context context, String title, String message) {
        final FlatDialog flatDialog = new FlatDialog(context);
        flatDialog.setTitle(title)
                .setBackgroundColor(context.getColor(R.color.black))
                .setSubtitle(message)
                .setFirstButtonText(context.getString(R.string.accept_continue))
                .setFirstButtonColor(context.getColor(R.color.primary_color))
                .withFirstButtonListner(view -> {
                    flatDialog.dismiss();
                })
                .show();
    }

    public void showSuccessMessage(Context context, String title, String message) {
        final FlatDialog flatDialog = new FlatDialog(context);
        flatDialog.setTitle(title)
                .setBackgroundColor(context.getColor(R.color.primary_color))
                .setSubtitle(message)
                .setFirstButtonText(context.getString(R.string.accept_continue))
                .setFirstButtonColor(context.getColor(R.color.black))
                .withFirstButtonListner(view -> {
                    flatDialog.dismiss();
                })
                .show();
    }

}
