package lazaro.io.codechallengexumac.character;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import lazaro.io.codechallengexumac.R;
import lazaro.io.codechallengexumac.character.model.Characters;
import lazaro.io.codechallengexumac.utils.Utils;

public class DetailCharacterActivity extends AppCompatActivity {

    private TextView tvName, tvOccupation, tvPortrayed, tvStatus;
    private ImageView ivCharacter;
    private AppCompatButton btnHeart;
    private final Utils utils = new Utils();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_character);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initVars();

        setCharacterData();
    }

    private void initVars() {
        // Textviews
        tvName = findViewById(R.id.tv_name_detail);
        tvOccupation = findViewById(R.id.tv_ocupation_detail);
        tvPortrayed = findViewById(R.id.tv_portrayed_detail);
        tvStatus = findViewById(R.id.tv_status_detail);

        // Imageviews
        ivCharacter = findViewById(R.id.iv_detail_character);

        // Buttons
        btnHeart = findViewById(R.id.btn_heart_detail);

        btnHeart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = getIntent().getExtras();
                if (bundle != null) {
                    // Verify if this character is currently in the favorites character database
                    int char_id = bundle.getInt("char_id");
                    String nickname = bundle.getString("nickname");
                    boolean isFav = new Characters().isFavorite(char_id);

                    if (isFav) {
                        new Characters().deleteByCharID(char_id);
                        btnHeart.setBackground(getResources().getDrawable(R.drawable.heart_shape));
                    } else {
                        Characters characters = new Characters(char_id);
                        characters.save();
                        btnHeart.setBackground(getResources().getDrawable(R.drawable.heart_shape_active));
                        utils.showSuccessMessage(btnHeart.getContext(), nickname + ", marked as favorite", "Character has been added to your favorites.");
                    }
                }

            }
        });
    }

    // Set character data from previous click on item (intent)
    private void setCharacterData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            RequestOptions options = new RequestOptions();
            options.placeholder(R.drawable.ic_launcher_background);
            options.error(android.R.drawable.stat_notify_error);
            Glide.with(this.ivCharacter)
                    .setDefaultRequestOptions(options)
                    .load(bundle.getString("url_image"))
                    .fitCenter()
                    .into(this.ivCharacter);

            // Set string values to show
            tvName.setText(bundle.getString("name"));
            tvOccupation.setText(bundle.getString("occupation"));
            tvStatus.setText(bundle.getString("status"));
            tvPortrayed.setText(bundle.getString("portrayed"));

            // Verify if this character is currently in the favorites character database
            boolean isCurrentlyFav = new Characters().isFavorite(bundle.getInt("char_id"));

            if (isCurrentlyFav) {
                btnHeart.setBackground(getResources().getDrawable(R.drawable.heart_shape_active));
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}