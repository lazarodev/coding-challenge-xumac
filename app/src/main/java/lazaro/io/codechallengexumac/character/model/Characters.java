package lazaro.io.codechallengexumac.character.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import java.util.List;

@Table(name = "Characters")
public class Characters extends Model {

    @Column(name = "char_id")
    public int char_id;

    public Characters() {
        super();
    }

    public Characters(int char_id) {
        super();
        this.char_id = char_id;
    }

    public static List<Characters> getAll() {
        return new Select()
                .from(Characters.class)
                .execute();
    }

    public boolean isFavorite(int char_id) {
        Characters user = new Select()
                .from(Characters.class)
                .where("char_id = ?", char_id)
                .executeSingle();

        if (user != null) {
            return true;
        }

        return false;
    }

    public void deleteByCharID(int char_id) {
        new Delete().from(Characters.class).where("char_id = ?", char_id).execute();
    }
}
