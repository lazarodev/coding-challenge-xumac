package lazaro.io.codechallengexumac.character;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.RecyclerView;

import com.activeandroid.query.Select;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import lazaro.io.codechallengexumac.R;
import lazaro.io.codechallengexumac.character.model.Characters;
import lazaro.io.codechallengexumac.utils.Utils;

public class CharacterAdapter extends RecyclerView.Adapter<CharacterAdapter.MyViewHolder> {

    private List<Character> characterList;
    private final Utils utils = new Utils();

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_character, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        final Character character = characterList.get(position);

        // Verify if this character is currently in the favorites character database
        boolean isCurrentlyFav = new Characters().isFavorite(character.getChar_id());

        if (isCurrentlyFav) {
            holder.btnHeart.setBackground(holder.btnHeart.getResources().getDrawable(R.drawable.heart_shape_active));
        }

        holder.tvName.setText(character.getName());
        holder.tvNickname.setText(character.getNickname());

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.breaking_bad_logo);
        requestOptions.error(android.R.drawable.stat_notify_error);
        Glide.with(holder.ivCharacter).
                setDefaultRequestOptions(requestOptions).
                load(character.getUrlImage())
                .fitCenter()
                .into(holder.ivCharacter);

        holder.linearCharacter.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putString("name", character.getName());
            bundle.putInt("char_id", character.getChar_id());
            bundle.putString("nickname", character.getNickname());
            bundle.putString("url_image", character.getUrlImage());
            bundle.putString("occupation", character.getOccupation());
            bundle.putString("status", character.getStatus());
            bundle.putString("portrayed", character.getPortrayed());
            v.getContext().startActivity(new Intent(v.getContext(), DetailCharacterActivity.class).putExtras(bundle));
        });

        holder.btnHeart.setOnClickListener(v -> {
            boolean isFav = new Characters().isFavorite(character.getChar_id());
            if (isFav) {
                new Characters().deleteByCharID(character.getChar_id());
                holder.btnHeart.setBackground(holder.btnHeart.getResources().getDrawable(R.drawable.heart_shape));
            } else {
                Characters characters = new Characters(character.getChar_id());
                characters.save();
                holder.btnHeart.setBackground(holder.btnHeart.getResources().getDrawable(R.drawable.heart_shape_active));
                utils.showSuccessMessage(holder.btnHeart.getContext(), character.getNickname() + ", marked as favorite", "Character has been added to your favorites.");
            }
        });
    }

    public static Characters getCharacterByID(int char_id) {
        return new Select().from(Characters.class).where("char_id = ?", char_id).executeSingle();
    }

    @Override
    public int getItemCount() {
        return characterList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvName;
        private final TextView tvNickname;
        private final ImageView ivCharacter;
        private final AppCompatButton btnHeart;
        private final LinearLayout linearCharacter;

        public MyViewHolder(View view) {
            super(view);

            linearCharacter = view.findViewById(R.id.linear_character);
            tvName = view.findViewById(R.id.tv_character_name);
            tvNickname = view.findViewById(R.id.tv_character_nickname);
            ivCharacter = view.findViewById(R.id.iv_character);
            btnHeart = view.findViewById(R.id.btn_heart);
        }
    }

    public CharacterAdapter(List<Character> characterList) {
        this.characterList = characterList;
    }

    public void clear() {
        int size = characterList.size();
        characterList.clear();
        notifyItemRangeRemoved(0, size);
    }

}
