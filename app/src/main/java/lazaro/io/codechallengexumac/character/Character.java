package lazaro.io.codechallengexumac.character;

public class Character {

    private String name, nickname, occupation, status, portrayed, urlImage;
    private int char_id;

    public Character(int char_id, String name, String nickname, String occupation, String status,
                     String portrayed, String urlImage) {
        this.char_id = char_id;
        this.name = name;
        this.nickname = nickname;
        this.occupation = occupation;
        this.status = status;
        this.portrayed = portrayed;
        this.urlImage = urlImage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPortrayed() {
        return portrayed;
    }

    public void setPortrayed(String portrayed) {
        this.portrayed = portrayed;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public int getChar_id() {
        return char_id;
    }

    public void setChar_id(int char_id) {
        this.char_id = char_id;
    }

}
